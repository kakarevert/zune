<div id="header">
    <div class="logo" style="text-align: center">
        <img src="images/logo.png" alt="" style="height: 100%">
    </div>
    <div class="container-fluid">
       <div class="category row ">
           <div class="col-sm-2"></div>
           <div class="col-sm-1 text-center"><a href="{{route('home')}}"><span class="zuna-text">HOME</span></a></div>
           <div class="col-sm-1 text-center">
               <a href="{{route('collections')}}"><span class="zuna-text">COLLECTIONS</span></a>
           </div>
           <div class="col-sm-1 text-center all_items">
               <a href="{{route('products')}}"><span class="zuna-text">ALL ITEMS</span></a>
               <div class="categories">
                   <div class="cate zuna-text">TOPS</div>
                   <div class="cate zuna-text">BOTTOMS</div>
                   <div class="cate zuna-text">ACCESSORIES</div>
               </div>
           </div>
           <div class="col-sm-1 text-center"><a href="{{route('sale')}}"><span class="zuna-text">SALES</span></a></div>
           <div class="col-sm-1 text-center"><a href="{{route('home')}}"><span class="zuna-text">#OOTD</span></a></div>
           <div class="col-sm-1 text-center"><a href="{{route('aboutUs')}}"><span class="zuna-text">ABOUT US</span></a></div>
           <div class="col-sm-1 text-center"><a href="{{route('news')}}"><span class="zuna-text">NEWS</span></a></div>
           <div class="col-sm-1 text-center cart"><a href="{{route('cart')}}"><span class="zuna-text"><i class="fas fa-shopping-cart"></i></span></a></div>
           <div class="col-sm-2"></div>
       </div>
        <div class="slide_layout">
            <div class="slide">
                <div class="image" style="background-image: url({{asset("images/image2.jpg")}})"></div>
                <div class="image" style="background-image: url({{asset("images/image3.jpg")}})"></div>
                <div class="image" style="background-image: url({{asset("images/image4.jpg")}})"></div>
                <div class="image" style="background-image: url({{asset("images/image2.jpg")}})"></div>
            </div>
            <div class="layout_berry">
                <div class="text"></div>
            </div>
        </div>
    </div>
</div>
