<div class="" id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-3 logo" >
                <img src="{{asset('images/logo.png')}}" alt="">
            </div>
            <div class="col-3 offset-1 social">
                <div class="contact_us title_footer">
                    <span>FOLLOW US</span>
                </div>
                <div class="icon">
                    <div><i class="fab fa-facebook-f"></i></div>
                    <div><i class="fab fa-instagram"></i></div>
                    <div><i class="fab fa-youtube"></i></div>
                </div>
            </div>
            <div class="col-5 contact">
                <div class="contact_us title_footer">
                    <span>CONTACT US</span>
                </div>
                <div class="address">
                    <span><i class="fas fa-map-marker-alt"></i>HA NOI: 131 Đông Các, Đống Đa</span><br>
                    <span><i class="fas fa-map-marker-alt"></i>HA NOI: Số 41 Tam Khương(ngõ Tôn Thất Tùng), Đống Đa</span> <br>
                    <span><i class="fas fa-map-marker-alt"></i>HA NOI: 131 Đông Các, Đống Đa</span><br>
                    <span><i class="fas fa-map-marker-alt"></i>HA NOI: 131 Đông Các, Đống Đa</span><br>
                    <span><i class="fas fa-phone"></i>0905498723</span> <br>
                    <span><i class="fas fa-envelope"></i>Lookbookzune@gmail.com</span>
                </div>
            </div>
        </div>
    </div>
</div>
