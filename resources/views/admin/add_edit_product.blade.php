@extends('layouts.master')

@section('content')

          <div class="panel-content">
              <form action=" <?php if(isset($product)){
                  echo route("admin.update.product",['id'=> $product->id]);
              } else {
                  echo route("admin.store.product");
              } ?>" method="post" enctype="multipart/form-data" >
              @csrf
              <div class="form-group">
                  <label for="name">Tên sản phẩm</label>
                  <input type="text" value="@if(isset($product)) {{ $product->name }} @endif" class="form-control" name="name" id="name" aria-describedby="emailHelp"
                         placeholder="Tên sản phẩm " required>
              </div>
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="price">Giá</label>
                          <input value="@if(isset($product)) {{ $product->price }} @endif" type="number" class="form-control" name="price" id="price"
                                 aria-describedby="emailHelp" placeholder="Nhập giá " required>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="price_sale">Giá khuyến mại</label>
                          <input  value="@if(isset($product)) {{ $product->sale_price }} @endif" type="number" class="form-control" name="sale_price" id="price_sale"
                                 aria-describedby="emailHelp" placeholder="Nhập giá">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="category">Category</label>
                          <select name="category_id" id="category_id" class="form-control" required>
                              <option disabled selected>Select one</option>
                              @foreach ($categories as $c)
                              <option @if(isset($product)&& $product->id == $c['id']) selected @endif value="{{ $c['id'] }}">{{$c['name']}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <input type="file" class="form-control" name="photos[]" multiple/>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <textarea value="@if(isset($product)) {{ $product->description }} @endif" name="description" id="description" cols="80" rows="10"></textarea>
                  </div>
              </div>
              <button type="submit" class="btn btn-primary">Save changes</button>
              </form>
          </div>
@endsection

@section('script')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    var mission = $('.content_mission').html();
    var d_introduce = $('.content_introduce').html();
    CKEDITOR.replace('content_introduce');
    CKEDITOR.replace('content_mission');
    CKEDITOR.replace('description');
    CKEDITOR.instances['content_introduce'].setData('' + d_introduce + '');
    CKEDITOR.instances['content_mission'].setData(mission);

    function getCategory() {
        alert("dfsdfsdfds");
        var url = {
        {
            route('admin.all.category')
        }
    }
        ;
        console.log(url)

    }
</script>
@endsection
