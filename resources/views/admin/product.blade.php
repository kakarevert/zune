@extends('layouts.master')

@section('content')
<div id="product">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right"><a href="{{ route("admin.create.product") }}">Thêm sản
                    phẩm</a></div>
        </div>
        <div class="container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $p)
                <tr>
                    <td>{{ $p['id'] }}</td>
                    <td>{{ $p['name'] }}</td>
                    <td>{{ $p->category['name'] }}</td>
                    <td><?php
                        $images = json_decode($p['images']);
                        foreach ($images as $image){
                            echo "<img width=100 height=100 src='".$image."' alt=\"\">";
                        }
                        ?></td>
                    <td><a href="{{ route("admin.edit.product",['id' => $p['id']])}}">Edit</a>&nbsp&nbsp<a href="{{ route("admin.delete.product",['id' => $p['id']])}}">Delete</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
@endsection

@section('script')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    var mission = $('.content_mission').html();
    var d_introduce = $('.content_introduce').html();
    CKEDITOR.replace('content_introduce');
    CKEDITOR.replace('content_mission');
    CKEDITOR.replace('description');
    CKEDITOR.instances['content_introduce'].setData('' + d_introduce + '');
    CKEDITOR.instances['content_mission'].setData(mission);

    function getCategory() {
        alert("dfsdfsdfds");
        var url = {
        {
            route('admin.all.category')
        }
    }
        ;
        console.log(url)

    }
</script>
@endsection
