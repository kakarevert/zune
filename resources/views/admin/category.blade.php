@extends('layouts.master')

@section('content')
<div id="category">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right"><a href="" data-toggle="modal" data-target="#modal_category">Thêm danh mục</a></div>
        </div>
        <div class="categories">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Danh mục</th>
                    <th scope="col">Danh mục 2</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($category as $cate)
                <tr>
                    <th scope="row">{{$cate['id']}}</th>
                    <td>{{$cate['name']}}</td>
                    <td></td>
                    <td><a href="{{route('admin.category.delete', $cate['id'])}}">Xoá</a></td>
                </tr>
                    @foreach($categories as $c)
                        @if($c['parent_id'] == $cate['id'])
                        <tr>
                            <th scope="row">{{$c['id']}}</th>
                            <td></td>
                            <td>{{$c['name']}}</td>
                            <td><a href="{{route('admin.category.delete', $cate['id'])}}">Xoá</a></td>
                        </tr>
                        @endif
                    @endforeach()
                @endforeach()

                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade " id="modal_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm Danh Mục</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route("admin.category.create")}}" method="post">
                <div class="form-group">

                    <div class="row">

                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Tên danh mục</label>
                                    <input type="text" name="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Nhập danh mục " required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category">Danh mục</label>
                                    <select name="parent_id" id="category" class="form-control">
                                        <option value="0" selected>None</option>
                                        @foreach($category as $cate)
                                        <option value="{{$cate['id']}}">{{$cate['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tạo </button>
            </div>
                </form>
        </div>
    </div>
</div>
    <div class="modal fade " id="update_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route("admin.category.create")}}" method="post">
                    <div class="form-group">

                        <div class="row">

                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Tên danh mục</label>
                                    <input type="text" name="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Nhập danh mục " required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category">Danh mục</label>
                                    <select name="parent_id" id="category" class="form-control">
                                        <option value="0" selected>None</option>
                                        @foreach($category as $cate)
                                        <option value="{{$cate['id']}}">{{$cate['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Tạo </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('script')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    var mission = $('.content_mission').html();
    var d_introduce = $('.content_introduce').html();
    CKEDITOR.replace('content_introduce');
    CKEDITOR.replace('content_mission');
    CKEDITOR.instances['content_introduce'].setData(''+d_introduce+'');
    CKEDITOR.instances['content_mission'].setData(mission);
</script>
@endsection
