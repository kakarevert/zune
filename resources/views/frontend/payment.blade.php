@extends('layouts.app')

@section('content')
<div id="payment">
    <div class="container">
        <div class="row">
            <div class="col-md-8 tab content">
                <ul class="nav nav-tabs" id="tab_payment">
                    <li class="active address_tab" style="width: 15%" data-id="address"><a >ĐỊA CHỈ</a></li>
                    <li class="shipping_tab" style="width: 20%" data-id="shipping"><a >SHIPPING</a></li>
                    <li class="payments_tab_tab" style="width: 25%" data-id="payments_tab"><a >THANH TOÁN</a></li>
                    <li class="confirm_tab" style="width: 40%" data-id="confirm"><a >XÁC NHẬN ĐƠN HÀNG</a></li>
                </ul>

                <div class="tab-content">
                    <div id="address" class="tab-pane fade in active show">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="name">*Họ và tên:</label>
                                <input type="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="*Họ và tên">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address">*Địa chỉ:</label>
                                <input type="address" class="form-control" id="address" aria-describedby="emailHelp" placeholder="*Địa chỉ">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address">*Email:</label>
                                <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="*Email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="city">*Thành phố:</label>
                                <input type="city" class="form-control" id="city" aria-describedby="emailHelp" placeholder="*Thành phố">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">*Số điện thoại:</label>
                                <input type="phone" class="form-control" id="phone" aria-describedby="emailHelp" placeholder="*Số điện thoại">
                            </div>
                        </div>
                        <div class=" shipping">
                            <div class="row">
                                <div class="title_cart col-md-12">
                                    <h6>Thông tin shipping</h6>
                                </div>
                            </div>
                           <div class="row info">
                               <div class="col-md-4">
                                   <div class="province">
                                       Tỉnh/Thành
                                   </div>
                                   <div class="">
                                       <select name="" id="" class="w-100">
                                           <option value="">Hà Nội</option>
                                       </select>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="district">
                                       Quận/Huyện
                                   </div>
                                   <div class="">
                                       <select name="" id="" class="w-100">
                                           <option value="">Đống Đa</option>
                                       </select>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="shipping_fee text-right">
                                       Chi phí
                                   </div>
                                   <div class="shipping_fee_price text-right">
                                       <span class="price">100.000</span><span>VND</span>
                                   </div>
                               </div>
                           </div>
                            <div class="row button">
                                <div class="col-md-12 float-right">
                                    <button class="button_berry" data-id="shipping">Tiếp tục</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="shipping" class="tab-pane fade in">
                        <div class="shipping_tab">
                            <label for="">Shipping</label>
                            <div class="">
                                <input type="radio" checked> Shipping <span>(Đống Đa - Hà Nội)</span> <span class="price">10.000 đ</span>
                            </div>
                        </div>
                        <div class="shipping_tab">
                            <label for="">Thêm ghi chú cho đơn hàng của bạn</label>
                            <div class="">
                                <textarea name="" id="" cols="30" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="shipping_tab button">
                           <div class="col-md-12">
                               <button class="button_berry" data-id="payments_tab">Tiếp tục</button>
                           </div>
                        </div>
                    </div>
                    <div id="payments_tab" class="tab-pane fade in">
                        <div class=" content_1">
                            <label for="">Hình thức thanh toán</label>
                            <div class="content">
                                <p>Vui lòng chọn phương thức thanh toán cho đơn hàng này</p>
                            </div>
                        </div>
                        <div class="content_2">
                            <label for="">Thêm ghi chú cho đơn hàng của bạn</label>
                        </div>
                        <div class="row content_3">
                            <div class="col-md-4 row-eq-height">
                               <div class="content">
                                   <label for="" class="font-weight-bold">Thanh toán khi nhận hàng</label>
                                   <div class="description">
                                       <p>Dành cho khách hàng tại Việt Nam. Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam</p>
                                   </div>
                               </div>
                                <div class="note row">
                                    <div class="input">
                                        <input type="radio" checked>
                                    </div>
                                    <div class="">
                                        <p class="">Thu phí 20000đ địa chỉ ngoài Hà Nội</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 row-eq-height">
                                <div class="content">
                                    <label for="">Chuyển khoản ngân hàng</label>
                                    <div class="description">
                                        <p>Dành cho khách hàng tại Việt Nam. Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam Dành cho khách hàng tại Việt Nam</p>
                                    </div>
                                </div>
                                <div class="note row">
                                    <div class="input">
                                        <input type="radio">
                                    </div>
                                    <div class="">
                                        Có phí chuyển <khoản></khoản>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 row-eq-height">
                                <div class="content">
                                    <label for="">VNPAY QR</label>
                                    <div class="description">
                                        <p>Dành cho khách hàng tại Việt Nam. Dành cho khách hàng tại Việt Nam</p>
                                        <img src="{{ asset('images/qr_code.png') }}" alt="">
                                    </div>
                                </div>
                                <div class="note row">
                                    <div class="input">
                                        <input type="radio">
                                    </div>
                                    <div class="">
                                        <p class="">VNPAY có thể thu phí</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="content_4">
                            <label for="">Thêm ghi chú cho đơn hàng của bạn</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="w-100" name="" id="" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="content_5 button">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="button_berry" data-id="confirm">Tiếp tục</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="confirm" class="tab-pane fade in">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="font-weight-bold" scope="col">Tên sản phẩm</th>
                                <th class="font-weight-bold" scope="col">Size</th>
                                <th class="font-weight-bold" scope="col">Số lượng</th>
                                <th class="font-weight-bold" scope="col" class="text-right">Giá</th>
                                <th class="font-weight-bold" scope="col" class="text-right">Tổng cộng</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row" class="font-weight-light">Áo Love is Blind</th>
                                <td class="text-center">M</td>
                                <td class="text-right">1</td>
                                <td class="text-right"><span class="price">100.000</span> VND</td>
                                <td class="text-right"><span class="price">100.000</span> VND</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="font-weight-bold text-right">Phí sản phẩm:</td>
                                    <td class="text-right"><span class="price">100.000</span> VND</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="font-weight-bold text-right">Shipping (Đống Đa-Hà Nội):	</td>
                                <td class="text-right"><span class="price">0</span> VND</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="font-weight-bold text-right">Tổng: </td>
                                <td class="text-right"><span class="price">100.000</span> VND</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="button row">
                            <div class="col-md-12">
                                <button class="button_berry">Xác nhận đơn hàng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 fee">
                <div class="tab title_cart">
                    <h6>Tổng phí order</h6>
                    <p>Phí shipping và thuế sẽ được tính khi checkout</p>
                    <div class="row">
                        <div class="key col">Phí sản phẩm:</div>
                        <div class="col value">100.000đ</div>
                    </div>
                    <div class="row">
                        <div class="key col">Shipping:</div>
                        <div class="col value">10.000đ</div>
                    </div>
                    <div class="row">
                        <div class="key col">Tổng:</div>
                        <div class="col value">110.000đ</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
        $('#payment').on('click', '.button_berry', function () {
            var id = $(this).data('id');
            $('#tab_payment li').removeClass('active');
            $('#tab_payment .'+id+'_tab').addClass('active');

            $('#payment .tab-pane').removeClass("active").removeClass("show");
            $('#payment #'+id+'').addClass('active').addClass("show");
        })
    </script>
    @endsection
