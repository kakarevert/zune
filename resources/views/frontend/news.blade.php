@extends('layouts.app')

@section('content')
    <div id="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="title">TIN TỨC</div>
                    <div class="news row">
                        @for ($i = 0; $i < 10; $i++)
                            <div class="col-6 col_new">
                                <div class="new">
                                    <div class="image">
                                        <img src="{{asset('images/image_not_found.jpg')}}" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</div>
                                        <div class="created">13/11/2019</div>
                                        <div class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    </div>
                <div class="col-lg-4 news_right">
                    @include("frontend.latest_news")
                </div>
            </div>
        </div>
    </div>
@endsection
