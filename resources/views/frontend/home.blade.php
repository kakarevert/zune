@extends('layouts.app')

@section("content")
<div id="colections">
    <div class="title">
        <span>COLLECTIONS</span>
    </div>
    <div class="images row">
        <div class="image col-4" style="background-image: url({{asset("images/image8.png")}})">
            <div class="new_collection">
                <div class="name">
                    SUMMER FALL
                </div>
                <div class="title">NEW COLLECTION 2019</div>
                <div class="line"></div>
            </div>
        </div>
        <div class="image col-4" style="background-image: url({{asset("images/image9.png")}})">
            <div class="new_collection">
                <div class="name">
                    SUMMER FALL
                </div>
                <div class="title">NEW COLLECTION 2019</div>
                <div class="line"></div>

            </div>
        </div>
        <div class="image col-4" style="background-image: url({{asset("images/image10.png")}})">
            <div class="new_collection">
                <div class="name">
                    SUMMER FALL
                </div>
                <div class="title">NEW COLLECTION 2019</div>
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>
<div id="videos">
    <div class="title">
        <span>VIDEOS</span>
    </div>
    <video controls>
        <source src="{{asset("videos/adidas.mp4")}}" type="video/mp4">
    </video>
</div>
<div id="lookbook">
    <div class="lookbook">
        <div class="title">
            <span>LOOKBOOK</span>
        </div>
        <div class="content">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
        </div>
    </div>
    <div class="images">
        <div class="image" style="background-image: url({{asset("images/image11.png")}})"></div>
        <div class="image" style="background-image: url({{asset("images/image12.png")}})"></div>
        <div class="image" style="background-image: url({{asset("images/image13.png")}})"></div>
    </div>
</div>
<div id="about">
    <div class="container-fluid">
    <div class="row">
        <div class="col-4">
            <div class="title">
                <span>ABOUT US</span>
            </div>
            <div class="content">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                <br>
                <br>

                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            </div>
        </div>
        <div class="col-7 offset-1">
            <div class="images">
                <div class="image image_small" >
                    <img src="{{asset("images/image7.png")}}" alt="">
                </div>
                <div class="image image_big" >
                    <img src="{{asset("images/image6.png")}}" alt="">
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
