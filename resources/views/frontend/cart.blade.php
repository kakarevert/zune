@extends('layouts.app')

@section("content")
<div id="cart">
	<div class="container">
		<div class="row">
			<div class="col-md-8 ">
				<div class="cart tab">
					<div class="title_cart">
						<h6>Giỏ hàng</h6>
						<p>Giỏ hàng của bạn có những sản phẩm sau</p>
					</div>
					<div class="products">
						<table class="table table-borderless">
							<thead>
							    <tr>
							      <th scope="col" style="width: 15%">Hình ảnh</th>
							      <th scope="col" style="width: 38%">Tên sản phẩm</th>
							      <th scope="col" style="width: 15%">Số lượng</th>
							      <th scope="col" style="width: 15%">Đơn giá</th>
							      <th scope="col" style="width: 17%">Tổng cộng</th>
							    </tr>
							</thead>
							  <tbody>
							    <tr>
							      <td><img src="images/image_not_found.jpg" alt="" style="width: 100%"></td>
							      <td>Love is blind</td>
							      <td>1</td>
							      <td>100000đ</td>
							      <td>100000đ</td>
							    </tr>
							  </tbody>
						</table>
					</div>
					<div class="button row">
                        <div class="col">
                            <button class="continue_buy">Tiếp tục mua hàng</button>
                        </div>
                        <div class="col">
                            <a href="{{route('payment')}}" class="pay button_berry">Thanh toán</a>
                        </div>
					</div>
				</div>
				<div class="shipping tab">
					<div class="title_cart">
						<h6>Ước tính shipping</h6>
						<p>Vui lòng nhập địa chỉ shipping để ước tính phí shipping</p>
					</div>
					<div class="address row">
						<div class="dictrict col-md-4">
							<label for="">Tỉnh thành</label>
							<select name="" id="">
								<option value="">Hà nội</option>
								<option value="">Tp Hồ Chí Minh</option>
							</select>
						</div>
						<div class="provice col-md-4">
							<label for="">Quận/Huyện</label>
							<select name="" id="">
								<option value="">Đống Đa</option>
								<option value="">Hai Bà Trưng</option>
							</select>
						</div>
						<div class="col-md-4">
							<label for="">Chi phí</label>
							<p>10.000 VND</p>
						</div>
					</div>
				</div>
				<div class="discount tab">
					<div class="title_cart">
						<h6>Sử dụng mã giảm giá</h6>
						<p>Nhập mã giảm giá tại đây</p>
						<div class="code">
							<div class="">
                                <input type="text" >
                            </div>
                            <div class="button">
                                <button class="button_berry">Áp dụng giảm giá</button>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 fee">
                <div class="tab title_cart">
                    <h6>Tổng phí order</h6>
                    <p>Phí shipping và thuế sẽ được tính khi checkout</p>
                    <div class="row">
                        <div class="key col">Phí sản phẩm:</div>
                        <div class="col value">100.000đ</div>
                    </div>
                    <div class="row">
                        <div class="key col">Shipping:</div>
                        <div class="col value">10.000đ</div>
                    </div>
                    <div class="row">
                        <div class="key col">Tổng:</div>
                        <div class="col value">110.000đ</div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection
