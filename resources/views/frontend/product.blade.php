@extends('layouts.app')
@section("content")
<div id="product">
    <div class="container">
        <div class="row">
            <div class="image col-lg-4">
                <img src="images/bomber.jpeg" alt="">
            </div>
            <div class="info col-lg-8">
                <div class="title_product">
                    LOVE IS BLIND
                </div>
                <div class="inventory_status">
                    Tình trạng: Còn hàng
                </div>
                <div class="price">
                    Giá: 500.000đ
                </div>
                <div class="sizes row">
                    <div class="text">
                        Size :
                    </div>
                    <div class="size">
                        <div >S</div>
                        <div>M</div>
                        <div>L</div>
                        <div>XL</div>
                    </div>
                </div>
                <div class="quantity">
                    <span>Số lượng: 1</span>
                </div>
                <div class="button">
                    <button >Mua hàng ngay</button>
                    <button >Thêm vào giỏ hàng</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="related_products">
                <div class="title">
                    SẢN PHẨM LIÊN QUAN
                </div>
                <div class="row products">
                    @for($i = 0; $i < 4; $i ++)
                        <div class="product">
                            <div class="image">
                                <img src="images/bomber.jpeg" alt="">
                            </div>
                            <div class="name">
                                LOVE IS BLIND
                            </div>
                            <div class="price">
                                500.000đ
                            </div>
                        </div>
                        @endfor
                </div>
                <div class="pagination">
                    <div class="page active"></div>
                    <div class="page"></div>
                    <div class="page"></div>
                    <div class="page"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
