@extends('layouts.app')
@section("content")
<div id="products">
    <div class="container-fluid">
        <div class="row">
            <div class="title text-center w-100">
                TẤT CẢ SẢN PHẨM
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-3 filter">
                        <div class="tops cate">
                            <div class="title_filter">TOPS</div>
                            <div class="categories">
                                <div class="category">HOODIE</div>
                                <div class="category">SWEATER</div>
                                <div class="category">JACKET</div>
                                <div class="category">T-SHIRT</div>
                                <div class="category">SHIRT</div>
                                <div class="category">OTHERS</div>
                            </div>
                        </div>
                        <div class="bottoms cate">
                            <div class="title_filter">BOTTOMS</div>
                            <div class="categories">
                                <div class="category">PANTS</div>
                                <div class="category">SHORTS</div>
                                <div class="category">SKIRT</div>
                            </div>
                        </div>
                        <div class="accessories cate">
                            <div class="title_filter">ACCESSORIES</div>
                        </div>
                        <div class="price_filter_1 cate">
                            <div class="title_filter_1">GIÁ SẢN PHẨM -</div>
                            <div><input type="checkbox"> <span>Dưới 500.000đ</span></div>
                            <div><input type="checkbox"> <span>500.000đ - 1.000.000đ</span></div>
                            <div><input type="checkbox"> <span>1.000.000đ - 2.000.000đ</span></div>
                            <div><input type="checkbox"> <span>2.000.000đ - 5.000.000đ</span></div>
                            <div><input type="checkbox"> <span>Trên 5.000.000đ</span></div>
                        </div>
                        <div class="colors cate">
                            <div class="title_filter_1">MÀU SẮC -</div>
                            <div class="row">
                                <div class="bg-white color"></div>
                                <div class="bg-primary color"></div>
                                <div class="bg-secondary color"></div>
                                <div class="bg-success color"></div>
                                <div class="bg-danger color"></div>
                                <div class="bg-warning color"></div>
                                <div class="bg-info color"></div>
                                <div class="bg-dark color"></div>
                            </div>
                        </div>
                        <div class="sizes cate">
                            <div class="title_filter_1">KÍCH THƯỚC -</div>
                            <div class="size">
                                <div>S</div>
                                <div>M</div>
                                <div>L</div>
                                <div>XL</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-9">
                        @for($i = 0; $i < 16; $i ++)
                            <a href="{{route('product')}}">
                            <div class="product float-left">

                                <div class="content w-100 h-100 ">
                                    <div class="buy_now">
                                        Mua hàng
                                    </div>
                                    <div class="image">
                                        <img src="images/bomber.jpeg" alt="">
                                    </div>
                                    <div class="text">
                                        <div class="name">
                                            LOVE IS BLIND
                                        </div>
                                        <div class="price">
                                            500.000 <span>đ</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
