@extends('layouts.app')

@section('content')
    <div id="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="new">
                        <div class="title_new">
                            How to Motivate Your Independent Sales Reps. How to Motivate Your Independent How to Motivate Your Independent
                        </div>
                        <div class="content_detail">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.

                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nos

                            <img src="{{asset('images/image11.png')}}" alt="">

                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.

                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nos

                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.

                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nos
                        </div>
                    </div>
                    <div class="tags">
                        <span class="tag">#hashtag</span>
                        <span class="tag">#hashtag</span>
                        <span class="tag">#hashtag</span>
                        <span class="tag">#hashtag</span>
                        <span class="tag">#hashtag</span>
                    </div>
                    <div class="related_news">
                        <div class="title">TIN TỨC LIÊN QUAN</div>
                        <div class="related_new">
                            <div class="new row">
                                <div class="col-3">
                                    <img src="{{asset('images/image_not_found.jpg')}}" alt="">
                                </div>
                                <div class="col-9 title_new">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                                </div>
                            </div>
                            <div class="new row">
                                <div class="col-3">
                                    <img src="{{asset('images/image_not_found.jpg')}}" alt="">
                                </div>
                                <div class="col-9 title_new">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                                </div>
                            </div>
                            <div class="new row">
                                <div class="col-3">
                                    <img src="{{asset('images/image_not_found.jpg')}}" alt="">
                                </div>
                                <div class="col-9 title_new">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 news_right">
                    @include("frontend.latest_news")
                </div>
            </div>
        </div>
    </div>
@endsection
