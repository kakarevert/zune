<div class="title">TIN TỨC MỚI NHẤT</div>
<div class="news row">
    @for ($i = 0; $i < 10; $i++)
        <div class="col-6 col_new">
            <div class="new">
                <div class="image">
                    <img src="{{asset('images/image_not_found.jpg')}}" alt="">
                </div>
                <div class="content">
                    <div class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</div>
                </div>
            </div>
        </div>
    @endfor
</div>
