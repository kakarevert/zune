@extends('layouts.app')

@section("content")
    <div id="about_us">
        <div class="container-fluid">
            <div class="row">
                <div class="timeline">
                    <div class="container left">
                        <div class="content">
                            <h2>2017</h2>
                            <p>Lorem ipsum..</p>
                        </div>
                    </div>
                    <div class="container right">
                        <div class="content">
                            <h2>2016</h2>
                            <p>Lorem ipsum..</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
