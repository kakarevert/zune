@extends('layouts.app')

@section("content")
<div id="collections_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 title">
                COLLECTIONS
            </div>
        </div>
        <div class="row collections">
          @for($i=0 ; $i < 3 ; $i ++)
                <div class="collection">
                    <div class="col-md-12 title_collection">
                        SUMMER FALL 2019
                    </div>
                    <div class="col-md-12 content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur dolorum et excepturi facilis, hic libero tenetur. A consequuntur, corporis cum dignissimos fugiat harum inventore maxime natus optio rerum suscipit veritatis!

                        <br>
                        <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio et exercitationem impedit iure laudantium nam nisi porro quam quasi tempore! Accusantium, sapiente, voluptatem. Ab et in non placeat sapiente voluptatum!
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{asset('images/image12.png')}}" alt="" class="w-100">
                            </div>
                            <div class="col-md-6">
                                <img src="{{asset('images/image13.png')}}" alt="" class="w-100">
                            </div>
                        </div>
                    </div>
                </div>
              @endfor
        </div>
    </div>
</div>

@endsection
