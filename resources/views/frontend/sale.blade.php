@extends('layouts.app')
@section("content")
<div id="sale_product">
    <div class="container">
        <div class="row">
            <div class="title">
                SẢN PHẨM SALE
            </div>
        </div>
        <div class="row">
            @for($i = 0 ; $i < 10; $i++)
                <div class="products">
                    <div class="product">
                        <div class="buy_now">
                            Mua hàng
                        </div>
                        <div class="image">
                            <div class="sale">
                                -10%
                            </div>
                            <img src="images/bomber.jpeg" alt="">
                        </div>
                        <div class="content">
                            <div class="name">
                                Bomber Adidas 2019
                            </div>
                            <div class="price row">
                                <div class="current_price col-6">
                                    550000 <span>đ</span>
                                </div>
                                <div class="sale_price col-6">
                                    500000 <span>đ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>

</div>
    @endsection
