@extends('backend.layout')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Category</h1>
	<button type="button" class="d-none d-md-inline-block btn btn-md btn-primary shadow-md" data-toggle="modal" data-target="#add-category">Add</button>
</div>
@if (session('status'))
	@if(isset(session('status')['false']))
	<div class="alert alert-danger">
	    {{ session('status')['false'] }}
	</div>
	@else
	<div class="alert alert-success">
	    {{ session('status')['success'] }}
	</div>
	@endif
@endif

<div class="card shadow mb-4">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th class="text-center">Action</th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{ $category->id }}</td>
						<td>{{ $category->name }}</td>
						<td class="text-center">
							<button class="btn btn-info edit-category" type="button" category_id="{{ $category->id }}"><i class="fas fa-edit"></i></button>
							<a href="#" class="btn btn-danger" role="button"><i class="fas fa-trash-alt"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="add-category" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{ route('addCategory') }}">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label for="email">Name:</label>
						<input required="" type="text" class="form-control" id="name" placeholder="Category name" name="name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="edit-category" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-edit-category" method="POST" action="">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label for="email">Name:</label>
						<input required="" type="text" class="form-control input-name" placeholder="Category name" name="name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
	  $(".edit-category").click(function(){
	  	var id = $(this).attr("category_id");

	  	$.get('{{ url("/admin/categories" ) }}'+'/'+id+'/json', function(data, status){
	  		var updateUrl = '{{ url("/admin/categories" ) }}'+'/'+id;
	  		$('#form-edit-category').attr('action',updateUrl);

	  		$('#form-edit-category .input-name').val(data.category.name);

	  		$('#edit-category').modal({
		        show: true
		    }); 
	    });
	  });
	});
</script>
@endsection