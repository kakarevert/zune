<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/cart', 'HomeController@cart')->name('cart');
Route::get('/collections', 'HomeController@collections')->name('collections');
Route::get('/news', 'HomeController@news')->name('news');
Route::get('/new', 'HomeController@new')->name('new');

Route::get('/sale', 'HomeController@sale')->name('sale');

Route::get('/products', 'HomeController@products')->name('products');
Route::get('/product', 'HomeController@product')->name('product');

Route::get('/payment', 'HomeController@payment')->name('payment');
Route::get('/aboutUs', 'HomeController@aboutUs')->name('aboutUs');


Route::get('/admin/size', 'AdminController@size')->name('admin.size');
Route::post('/admin/size', 'AdminController@createSize')->name('admin.size.create');


Route::prefix('admin')->group(function () {
    Route::get('/', function () {
       return view("layouts/master");
    });

    Route::prefix("categorys")->group(function (){
        Route::get("/","CategoryController@index")->name('admin.category');
       Route::post("/","CategoryController@store")->name('admin.category.create');
        Route::get('/delete/{id}', 'CategoryController@destroy')->name('admin.category.delete');
    });

    Route::prefix("products")->group(function (){
        Route::get('/', 'ProductController@adminIndex')->name('admin.product');
        Route::get('/create', 'ProductController@create')->name('admin.create.product');
        Route::post('/create', 'ProductController@store')->name('admin.store.product');
        Route::get('/update/{id}', 'ProductController@edit')->name('admin.edit.product');
        Route::post('/update/{id}', 'ProductController@update')->name('admin.update.product');
        Route::get('/delete/{id}', 'ProductController@adminIndex')->name('admin.delete.product');
    });

    Route::get('/size', 'AdminController@size')->name('admin.size');
    Route::post('/size', 'AdminController@createSize')->name('admin.size.create');
});

