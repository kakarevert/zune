<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\Size;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function product()
    {
        $products = Product::all();
        return view('admin.product', compact('products'));
    }

    public function size()
    {
        $size = Size::all();
        return view('admin.size', compact('size'));
    }

    public function createSize(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        Size::create($data);
        $size = Size::all()->toArray();
        return redirect(route("admin.size"))->with(compact('size'));
    }
}
