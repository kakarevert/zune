<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        $categories = Category::all()->toArray();
        $products = Product::all();
        return view('admin.product', compact('products', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->toArray();
        return view('admin.add_edit_product',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $images = array();

        $files = $request->file('photos');
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $name = str_replace(' ', '_', uniqid() . '_' . time() . '.' . $file->getClientOriginalName());
            $filePath = $file->move('product_image', $name);
            array_push($images, $filePath->getLinkTarget());
        }

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->description = $request->description;
        $product->images = json_encode($images);
        $product->category_id = $request->category_id;

        $product->save();

        return redirect()->route('admin.product');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all()->toArray();
        $product = Product::find($id);
        return view('admin.add_edit_product',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $images = array();

        $files = $request->file('photos');
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $name = str_replace(' ', '_', uniqid() . '_' . time() . '.' . $file->getClientOriginalName());
            $filePath = $file->move('product_image', $name);
            array_push($images, $filePath->getLinkTarget());
        }

        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->description = $request->description;
        $product->images = json_encode($images);
        $product->category_id = $request->category_id;

        $product->save();

        return redirect()->route('admin.product');
    }

    private function validateProduct(Request $request){
        return $request->validate([
            'name' => 'required',
            'price' => 'required',
            'sale_price' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'images' => 'required',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
