<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class HomeController extends Controller
{
     public function home()
    {
        return view('frontend.home');
    }

     public function cart()
    {
        return view('frontend.cart');
    }
    public function collections()
    {
        return view('frontend.collections');
    }

    public function news()
    {
        return view('frontend.news');
    }
    public function new()
    {
        return view('frontend.new');
    }
    public function sale()
    {
        return view('frontend.sale');
    }

    public function products()
    {
        return view('frontend.products');
    }

    public function product()
    {
        return view('frontend.product');
    }

    public function payment()
    {
        return view('frontend.payment');
    }

    public function aboutUs()
    {
        return view('frontend.home');
    }
}
